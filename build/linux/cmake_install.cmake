# Install script for directory: /home/aaa/ogz-source/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/aaa/ogz-source/src/../install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/aaa/ogz-source/build/linux/sdk/pevents/cmake_install.cmake")
  include("/home/aaa/ogz-source/build/linux/sdk/rapidxml/cmake_install.cmake")
  include("/home/aaa/ogz-source/build/linux/sdk/rapidjson/cmake_install.cmake")
  include("/home/aaa/ogz-source/build/linux/sdk/ini/cmake_install.cmake")
  include("/home/aaa/ogz-source/build/linux/cml/cmake_install.cmake")
  include("/home/aaa/ogz-source/build/linux/launcher/cmake_install.cmake")
  include("/home/aaa/ogz-source/build/linux/PatchCreator/cmake_install.cmake")
  include("/home/aaa/ogz-source/build/linux/CSCommon/cmake_install.cmake")
  include("/home/aaa/ogz-source/build/linux/RealSpace2/cmake_install.cmake")
  include("/home/aaa/ogz-source/build/linux/SafeUDP/cmake_install.cmake")
  include("/home/aaa/ogz-source/build/linux/Locator/cmake_install.cmake")
  include("/home/aaa/ogz-source/build/linux/MatchServer/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/aaa/ogz-source/build/linux/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
