# CMake generated Testfile for 
# Source directory: /home/aaa/ogz-source/src
# Build directory: /home/aaa/ogz-source/build/linux
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("sdk/pevents")
subdirs("sdk/rapidxml")
subdirs("sdk/rapidjson")
subdirs("sdk/ini")
subdirs("cml")
subdirs("launcher")
subdirs("PatchCreator")
subdirs("CSCommon")
subdirs("RealSpace2")
subdirs("SafeUDP")
subdirs("Locator")
subdirs("MatchServer")
